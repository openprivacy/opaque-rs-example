import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import "opaque" as Opaque
import "opaque/fonts"
import "opaque/theme"

ApplicationWindow {
    id: windowItem
    width: 1200
    height: 800
    visible: true
    title: "cadnocudd" + ""
    font.family: Fonts.applicationFontRegular.name
    font.styleName: "Light"

    RowLayout {
        width:parent.width
        height:parent.height

        Rectangle { // THE LEFT PANE WITH TOOLS AND CONTACTS
            color: Theme.backgroundMainColor
            width:parent.width
            height:parent.height
            Layout.fillHeight: true
            Layout.minimumWidth: Layout.maximumWidth
            Layout.maximumWidth: parent.width
            visible:true
        }

        Opaque.SettingsList {  // groupSettingsPane
            id: gsp
            width:parent.width
            height:parent.height


            property string serverName: gcd.os

            settings: Column {
                width:parent.width
                Opaque.Setting {
                    inline: true
                    label: qsTr("server-label")

                    field: Opaque.ButtonTextField {
                        id: txtServer
                        readOnly: true
                        icon: "regular/copy"
                        text: gsp.serverName;
                        button_text: qsTr("copy-btn")
                        dropShadowColor: Theme.dropShadowPaneColor
                        onClicked: {
                            //: notification: copied to clipboard
                            //gcd.popup(qsTr("copied-to-clipboard-notification"))
                            txtServer.selectAll()
                            txtServer.copy()
                        }
                    }
                }

                                              Column {
                                                  width:parent.width * 0.95
                                                  anchors.horizontalCenter: parent.horizontalCenter
                                                  Opaque.Button {
                                                      icon: "regular/trash-alt"
                                                      text: qsTr("delete-btn")
                                                      anchors.right: parent.right


                                                      onClicked: {
                                                          gcd.theme =  gcd.theme == "light" ? "dark" : "light"
                                                      }
                                                  }
                                              }

           }



           }
    }



}