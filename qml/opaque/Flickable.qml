//import QtGraphicalEffects 1.0
//import QtQuick 2.7
//import QtQuick.Controls 2.4
//import QtQuick.Controls.Material 2.0

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12

import QtQuick.Templates 2.12 as T

//import QtQuick.Layouts 1.3

import "../opaque" as Opaque
import "../opaque/theme"
import "../const"

Flickable {
    id: sv
    clip: true

    boundsBehavior: Flickable.StopAtBounds

    ScrollBar.vertical: Opaque.ScrollBar {}
}
