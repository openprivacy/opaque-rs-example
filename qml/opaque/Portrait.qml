import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import "theme"

Item {
    id: imgProfile
    implicitWidth: baseWidth
    implicitHeight: baseWidth

    property string source
    property alias badgeColor: badge.color

    property int size: Theme.contactPortraitSize
    property real logscale: 4 * Math.log10(gcd.themeScale + 1)
    property int baseWidth:  size * logscale
    height: size * logscale

    property alias portraitBorderColor: mainImage.color
    property alias portraitColor: imageInner.color
    property alias badgeVisible: badge.visible
    property alias badgeContent: badge.content


    Rectangle {
        id: mainImage
        //anchors.leftMargin: baseWidth * 0.1
        anchors.horizontalCenter: parent.horizontalCenter
        width: baseWidth * 0.8
        height: width
        anchors.verticalCenter: parent.verticalCenter
        color:  Theme.portraitOfflineBorderColor
        radius: width / 2

        Rectangle {
            id: imageInner
            width: parent.width - 4
            height: width
            color:  Theme.portraitOfflineBorderColor
            radius: width / 2
            anchors.centerIn:parent

            Image { // PROFILE IMAGE
                id: img
                source: imgProfile.source == "" ? "" : gcd.assetPath + imgProfile.source
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                visible: false

                sourceSize.width: imageInner.width*2
                sourceSize.height: imageInner.height*2
            }

            Image { // CIRCLE MASK
                id: mask
                fillMode: Image.PreserveAspectFit
                visible: false
                source: "qrc:/qml/opaque/images/clipcircle.png"
            }

            OpacityMask {
                anchors.fill: img
                source: img
                maskSource: mask
            }
        }
    }

    Badge {
        id: badge
        size: parent.width * 0.25
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: parent.width * 0.09
    }
}
