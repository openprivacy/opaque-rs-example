<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>EmojiDrawer</name>
    <message>
        <location filename="../EmojiDrawer.qml" line="190"/>
        <source>cycle-cats-android</source>
        <translation>Click to cycle category.
        Long-press to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="190"/>
        <source>cycle-cats-desktop</source>
        <translation>Click to cycle category.
        Right-click to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-morphs-android</source>
        <translation>Click to cycle morphs.
        Long-press to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-morphs-desktop</source>
        <translation>Click to cycle morphs.
        Right-click to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="258"/>
        <source>cycle-colours-android</source>
        <translation>Click to cycle colours.
        Long-press to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="258"/>
        <source>cycle-colours-desktop</source>
        <translation>Click to cycle colours.
        Right-click to reset.</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="64"/>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation>Search...</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="76"/>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation>Expressions</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="84"/>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation>Activities</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="92"/>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation>Food, drink &amp; herbs</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="100"/>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation>Gender, relationships &amp; sexuality</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="108"/>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation>Nature and effects</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="116"/>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation>Objects</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="124"/>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation>People and animals</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="132"/>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation>Symbols</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="140"/>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation>Travel &amp; places</translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="148"/>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation>Miscellaneous</translation>
    </message>
</context>
</TS>
