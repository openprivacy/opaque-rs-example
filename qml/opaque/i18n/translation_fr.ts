<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>EmojiDrawer</name>
    <message>
        <location filename="../EmojiDrawer.qml" line="64"/>
        <source>search</source>
        <extracomment>Search...</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="76"/>
        <source>emojicat-expressions</source>
        <extracomment>Expressions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="84"/>
        <source>emojicat-activities</source>
        <extracomment>Activities</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="92"/>
        <source>emojicat-food</source>
        <extracomment>Food, drink &amp; herbs</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="100"/>
        <source>emojicat-gender</source>
        <extracomment>Gender, relationships &amp; sexuality</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="108"/>
        <source>emojicat-nature</source>
        <extracomment>Nature and effects</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="116"/>
        <source>emojicat-objects</source>
        <extracomment>Objects</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="124"/>
        <source>emojicat-people</source>
        <extracomment>People and animals</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="132"/>
        <source>emojicat-symbols</source>
        <extracomment>Symbols</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="140"/>
        <source>emojicat-travel</source>
        <extracomment>Travel &amp; places</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="148"/>
        <source>emojicat-misc</source>
        <extracomment>Miscellaneous</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="190"/>
        <source>cycle-cats-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="190"/>
        <source>cycle-cats-desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-morphs-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="225"/>
        <source>cycle-morphs-desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="258"/>
        <source>cycle-colours-android</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../EmojiDrawer.qml" line="258"/>
        <source>cycle-colours-desktop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
