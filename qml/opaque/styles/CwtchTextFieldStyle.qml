import QtQuick.Controls.Styles 1.4
import QtQuick 2.7
import "../theme"

TextFieldStyle {
    id: root
    textColor: "black"
    font.pointSize: 10 * gcd.themeScale
    property int width: parent.width

    background: Rectangle {
        radius: 2
        color: Theme.backgroundPaneColor
        border.color: Theme.defaultButtonActiveColor//??
    }
}
