import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


Rectangle {
    id: pb
    border.color: "#FFFFFF"
    border.width: 1

    color: "#D2C0DD"
    property bool running

    // Indeterminate animation by animating alternating stripes:
    Item {
        anchors.fill: parent
        anchors.margins: 1
        visible: control.indeterminate
        clip: true
        Row {
            Repeater {
                Rectangle {
                    color: index % 2 ? "#D2C0DD" : "#b29dbe"
                    width: 20 ; height: control.height
                }
                model: control.width / 20 + 2
            }
            XAnimator on x {
                from: 0 ; to: -40
                loops: Animation.Infinite
                running: pb.running
            }
        }
    }
}
