import QtQuick.Controls.Styles 1.4
import QtQuick 2.7
import "../theme"

TextAreaStyle {
    textColor: "black"
    backgroundColor: Theme.backgroundPaneColor
}
