import QtQuick 2.7

import QtQuick.Controls 2.13
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.12
import "." as Widgets
import "theme"

// IconTextField integrates a text field and an icon
TextField {
    color:  Theme.textfieldTextColor
    font.pixelSize: Theme.secondaryTextSize * gcd.themeScale * gcd.themeScale
    width: parent.width - 20
    property alias icon: icon_.source
    signal clicked
    smooth: true
    placeholderTextColor: Theme.altTextColor


    background: Rectangle {
        radius: 10
        color: Theme.textfieldBackgroundColor
        border.color: Theme.textfieldBorderColor
        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            horizontalOffset: 0
            verticalOffset: 0
            samples: 10
            radius: 8
            color: Theme.dropShadowColor
        }
    }

    Widgets.Icon {
        id: icon_

        anchors.right: parent.right
        anchors.rightMargin: 4
        anchors.verticalCenter: parent.verticalCenter

        height: parent.height-4;
        width: parent.height-4;

        iconColor: Theme.textfieldTextColor
        backgroundColor: Theme.textfieldBackgroundColor
    }

}
