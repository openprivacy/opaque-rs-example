import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "fonts/Twemoji.js" as T
import "theme"
import "fonts"

Rectangle {
    id: button
    width: (text == undefined || text == "" ? 0 : buttonText.width) + (icon == undefined || icon == "" ? 0 : ico.width) + 24 * gcd.themeScale
    Layout.minimumWidth: width
    Layout.maximumWidth: width
    height: 20 * gcd.themeScale
    Layout.minimumHeight: height
    Layout.maximumHeight: height
    property color inactiveColor: Theme.defaultButtonColor
    property color activeColor: Theme.defaultButtonActiveColor
    property color textColor: Theme.defaultButtonTextColor
    color: mousedown ? activeColor : inactiveColor
    border.color:  mousedown ? activeColor : inactiveColor
    border.width: 1
    radius: override_radius
    antialiasing: true

    property bool checked: false
    property double override_radius:  (height / 2.0)
    property alias text: buttonText.text
    property alias font: buttonText.font.family
    property string icon
    property bool mousedown
    property string tooltip
    signal clicked

    RowLayout {
        anchors.centerIn: parent
        spacing: 6 * gcd.themeScale

        Image {
            id: ico
            source: icon!="" ? gcd.assetPath + "fontawesome/"+icon+".svg" : "";
            visible: icon != ""
            height: button.height / 2
            sourceSize.height: button.height / 2
        }

        Label {
            id: buttonText
            font.family: Fonts.applicationFontRegular.name
            font.styleName: "ExtraBold"
            font.pixelSize: button.height / 2
            color: button.textColor
            visible: button.text != "" && button.text != undefined
        }

        ToolTip.visible: tooltip != "" && mouseArea.containsMouse
        ToolTip.text: tooltip
    }


    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
            parent.focus = true
            parent.clicked()
        }

        onPressed: mousedown = true

        onReleased: mousedown = false

        hoverEnabled: true
    }

    Keys.onSpacePressed: clicked()
}
