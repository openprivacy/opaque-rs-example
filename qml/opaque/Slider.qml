import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12
import QtGraphicalEffects 1.12
import "theme"

Slider {
    id: control

    property color buttonColor: Theme.sliderButtonColor
    property color buttonActiveColor: Theme.defaultButtonActiveColor
    property color barRightColor: Theme.sliderBarRightColor
    property color barLeftColor: Theme.sliderBarLeftColor

    background: Rectangle {
        x: control.leftPadding
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 200
        implicitHeight: 4
        width: control.availableWidth
        height: implicitHeight
        radius: 2
        color: control.barRightColor

        Rectangle {
            width: control.visualPosition * parent.width
            height: parent.height
            color: control.barLeftColor
            radius: 2
        }
    }

    handle: Rectangle {
        x: control.leftPadding + control.visualPosition * (control.availableWidth - width)
        y: control.topPadding + control.availableHeight / 2 - height / 2
        implicitWidth: 26 * gcd.themeScale
        implicitHeight: 26 * gcd.themeScale
        radius: 13 * gcd.themeScale
        color: control.pressed ? control.buttonActiveColor : control.buttonColor
        border.color: control.buttonColor
    }

}
