
ThemeType {
    readonly property color darkGreyPurple: "#281831"
    readonly property color deepPurple: "#422850"
    readonly property color mauvePurple: "#8E64A5"
    readonly property color purple: "#DFB9DE"
    readonly property color whitePurple: "#E3DFE4"
    readonly property color softPurple: "#FDF3FC"
    readonly property color pink: "#E85DA1"
    readonly property color hotPink: "#D01972"
    readonly property color lightGrey: "#9E9E9E"
    readonly property color softGreen: "#A0FFB0"

    backgroundMainColor: darkGreyPurple
    backgroundPaneColor: darkGreyPurple
    backgroundHilightElementColor: deepPurple

    dividerColor: deepPurple

    mainTextColor: whitePurple
    altTextColor: whitePurple
    hilightElementTextColor: purple
    defaultButtonColor: hotPink
    defaultButtonActiveColor: pink
    defaultButtonTextColor: whitePurple
    defaultButtonDisabledColor: deepPurple
    defaultButtonDisabledTextColor: darkGreyPurple
    altButtonColor: darkGreyPurple
    altButtonTextColor: purple
    altButtonDisabledColor: darkGreyPurple
    altButtonDisabledTextColor: purple

    textfieldBackgroundColor: deepPurple
    textfieldBorderColor: deepPurple
    textfieldTextColor: purple
    textfieldButtonColor: purple
    textfieldButtonTextColor: darkGreyPurple

    portraitOnlineBorderColor: whitePurple
    portraitOnlineBackgroundColor: whitePurple
    portraitOnlineTextColor: whitePurple
    portraitConnectingBorderColor: purple //mauvePurple
    portraitConnectingBackgroundColor: purple //darkGreyPurple
    portraitConnectingTextColor: whitePurple
    portraitOfflineBorderColor: purple
    portraitOfflineBackgroundColor: purple
    portraitOfflineTextColor: purple
    portraitBlockedBorderColor: lightGrey
    portraitBlockedBackgroundColor: lightGrey
    portraitBlockedTextColor: lightGrey

    portraitOnlineBadgeColor: softGreen

    portraitContactBadgeColor: hotPink
    portraitContactBadgeTextColor: whitePurple
    portraitProfileBadgeColor: mauvePurple
    dropShadowColor: mauvePurple
    dropShadowPaneColor: darkGreyPurple

    toggleColor: darkGreyPurple
    toggleOnColor: whitePurple
    toggleOffColor: deepPurple
    sliderButtonColor: whitePurple
    sliderBarLeftColor: mauvePurple
    sliderBarRightColor: mauvePurple
    boxCheckedColor: hotPink

    toolbarIconColor: whitePurple
    toolbarMainColor: darkGreyPurple
    toolbarAltColor: deepPurple

    statusbarDisconnectedInternetColor: whitePurple
    statusbarDisconnectedInternetFontColor: deepPurple
    statusbarDisconnectedTorColor: darkGreyPurple
    statusbarDisconnectedTorFontColor: whitePurple
    statusbarConnectingColor: deepPurple
    statusbarConnectingFontColor: whitePurple
    statusbarOnlineColor: mauvePurple
    statusbarOnlineFontColor: whitePurple

    messageFromMeBackgroundColor: mauvePurple
    messageFromMeTextColor: whitePurple
    messageFromOtherBackgroundColor: deepPurple
    messageFromOtherTextColor: whitePurple

    messageStatusNormalColor: deepPurple
    messageStatusBlockedColor: lightGrey
    messageStatusBlockedTextColor: whitePurple
    messageStatusAlertColor: mauvePurple
    messageStatusAlertTextColor: whitePurple
}
