
ThemeType {
    readonly property color whitePurple: "#FFFDFF"
    readonly property color softPurple: "#FDF3FC"
    readonly property color purple: "#DFB9DE"
    readonly property color brightPurple: "#760388"
    readonly property color darkPurple: "#350052"
    readonly property color greyPurple: "#775F84"
    readonly property color pink: "#E85DA1"
    readonly property color hotPink: "#D01972"
    readonly property color lightGrey: "#B3B6B3"
    readonly property color softGreen: "#A0FFB0"

    backgroundMainColor: whitePurple
    backgroundPaneColor: softPurple
    backgroundHilightElementColor: softPurple

    dividerColor: purple

    mainTextColor: darkPurple
    altTextColor: purple
    hilightElementTextColor: darkPurple

    defaultButtonColor: hotPink
    defaultButtonActiveColor: pink
    defaultButtonTextColor: whitePurple
    defaultButtonDisabledColor: purple
    defaultButtonDisabledTextColor: whitePurple
    altButtonColor: whitePurple
    altButtonTextColor: purple
    altButtonDisabledColor: softPurple
    altButtonDisabledTextColor: purple

    textfieldBackgroundColor: whitePurple
    textfieldBorderColor: purple
    textfieldTextColor: purple
    textfieldButtonColor: hotPink
    textfieldButtonTextColor: whitePurple

    portraitOnlineBorderColor: darkPurple
    portraitOnlineBackgroundColor: darkPurple
    portraitOnlineTextColor: darkPurple
    portraitConnectingBorderColor: greyPurple
    portraitConnectingBackgroundColor: greyPurple
    portraitConnectingTextColor: greyPurple
    portraitOfflineBorderColor: greyPurple //purple
    portraitOfflineBackgroundColor: greyPurple //purple
    portraitOfflineTextColor: greyPurple//purple
    portraitBlockedBorderColor: lightGrey
    portraitBlockedBackgroundColor: lightGrey
    portraitBlockedTextColor: lightGrey

    portraitOnlineBadgeColor: softGreen

    portraitContactBadgeColor: hotPink
    portraitContactBadgeTextColor: whitePurple
    portraitProfileBadgeColor: brightPurple
    dropShadowColor: purple
    dropShadowPaneColor: purple

    toggleColor: whitePurple
    toggleOnColor: hotPink
    toggleOffColor: purple
    sliderButtonColor: pink
    sliderBarLeftColor: purple
    sliderBarRightColor: purple
    boxCheckedColor: darkPurple

    toolbarIconColor: darkPurple
    toolbarMainColor: whitePurple
    toolbarAltColor: softPurple

    statusbarDisconnectedInternetColor: softPurple
    statusbarDisconnectedInternetFontColor: darkPurple
    statusbarDisconnectedTorColor: purple
    statusbarDisconnectedTorFontColor: darkPurple
    statusbarConnectingColor: greyPurple
    statusbarConnectingFontColor: whitePurple
    statusbarOnlineColor: darkPurple
    statusbarOnlineFontColor: whitePurple

    messageFromMeBackgroundColor: darkPurple
    messageFromMeTextColor: whitePurple
    messageFromOtherBackgroundColor: purple
    messageFromOtherTextColor: darkPurple

    messageStatusNormalColor: purple
    messageStatusBlockedColor: lightGrey
    messageStatusBlockedTextColor: whitePurple
    messageStatusAlertColor: hotPink
    messageStatusAlertTextColor: whitePurple
}
