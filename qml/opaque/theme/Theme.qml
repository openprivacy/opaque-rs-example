pragma Singleton

import QtQuick 2.0

Item {
    readonly property color backgroundMainColor: theme.backgroundMainColor
    readonly property color backgroundPaneColor: theme.backgroundPaneColor
    readonly property color backgroundHilightElementColor: theme.backgroundHilightElementColor

    readonly property color dividerColor: theme.dividerColor

    readonly property color mainTextColor: theme.mainTextColor
    readonly property color altTextColor: theme.altTextColor
    readonly property color hilightElementTextColor: theme.hilightElementTextColor

    readonly property color defaultButtonColor: theme.defaultButtonColor
    readonly property color defaultButtonActiveColor: theme.defaultButtonActiveColor
    readonly property color defaultButtonTextColor: theme.defaultButtonTextColor
    readonly property color defaultButtonDisabledColor: theme.defaultButtonDisabledColor
    readonly property color defaultButtonDisabledTextColor: theme.defaultButtonDisabledTextColor
    readonly property color altButtonColor: theme.altButtonColor
    readonly property color altButtonTextColor: theme.altButtonTextColor
    readonly property color altButtonDisabledColor: theme.altButtonDisabledColor
    readonly property color altButtonDisabledTextColor: theme.altButtonDisabledTextColor

    readonly property color textfieldBackgroundColor: theme.textfieldBackgroundColor
    readonly property color textfieldBorderColor: theme.textfieldBorderColor
    readonly property color textfieldTextColor: theme.textfieldTextColor
    readonly property color textfieldButtonColor: theme.textfieldButtonColor
    readonly property color textfieldButtonTextColor: theme.textfieldButtonTextColor


    readonly property color dropShadowColor: theme.dropShadowColor
    readonly property color dropShadowPaneColor: theme.dropShadowPaneColor

    readonly property color portraitOnlineBorderColor: theme.portraitOnlineBorderColor
    readonly property color portraitOnlineBackgroundColor: theme.portraitOnlineBackgroundColor
    readonly property color portraitOnlineTextColor: theme.portraitOnlineTextColor
    readonly property color portraitConnectingBorderColor: theme.portraitConnectingBorderColor
    readonly property color portraitConnectingBackgroundColor: theme.portraitConnectingBackgroundColor
    readonly property color portraitConnectingTextColor: theme.portraitConnectingTextColor
    readonly property color portraitOfflineBorderColor: theme.portraitOfflineBorderColor
    readonly property color portraitOfflineBackgroundColor: theme.portraitOfflineBackgroundColor
    readonly property color portraitOfflineTextColor: theme.portraitOfflineTextColor
    readonly property color portraitBlockedBorderColor: theme.portraitBlockedBorderColor
    readonly property color portraitBlockedBackgroundColor: theme.portraitBlockedBackgroundColor
    readonly property color portraitBlockedTextColor: theme.portraitBlockedTextColor

    readonly property color portraitOnlineBadgeColor: theme.portraitOnlineBadgeColor

    readonly property color portraitContactBadgeColor: theme.portraitContactBadgeColor
    readonly property color portraitContactBadgeTextColor: theme.portraitContactBadgeTextColor
    readonly property color portraitProfileBadgeColor: theme.portraitProfileBadgeColor

    readonly property color toggleColor: theme.toggleColor
    readonly property color toggleOffColor: theme.toggleOffColor
    readonly property color toggleOnColor: theme.toggleOnColor
    readonly property color sliderButtonColor: theme.sliderButtonColor
    readonly property color sliderBarLeftColor: theme.sliderBarLeftColor
    readonly property color sliderBarRightColor: theme.sliderBarRightColor
    readonly property color boxCheckedColor: theme.boxCheckedColor


    readonly property color toolbarIconColor: theme.toolbarIconColor
    readonly property color toolbarMainColor: theme.toolbarMainColor
    readonly property color toolbarAltColor: theme.toolbarAltColor

    readonly property color statusbarDisconnectedInternetColor: theme.statusbarDisconnectedInternetColor
    readonly property color statusbarDisconnectedInternetFontColor: theme.statusbarDisconnectedInternetFontColor
    readonly property color statusbarDisconnectedTorFontColor: theme.statusbarDisconnectedTorFontColor
    readonly property color statusbarDisconnectedTorColor: theme.statusbarDisconnectedTorColor
    readonly property color statusbarConnectingColor: theme.statusbarConnectingColor
    readonly property color statusbarConnectingFontColor: theme.statusbarConnectingFontColor
    readonly property color statusbarOnlineColor: theme.statusbarOnlineColor
    readonly property color statusbarOnlineFontColor: theme.statusbarOnlineFontColor

    readonly property color messageFromMeBackgroundColor: theme.messageFromMeBackgroundColor
    readonly property color messageFromMeTextColor: theme.messageFromMeTextColor
    readonly property color messageFromOtherBackgroundColor: theme.messageFromOtherBackgroundColor
    readonly property color messageFromOtherTextColor: theme.messageFromOtherTextColor

    readonly property color messageStatusNormalColor: theme.messageStatusNormalColor
    readonly property color messageStatusBlockedColor: theme.messageStatusBlockedColor
    readonly property color messageStatusBlockedTextColor: theme.messageStatusBlockedTextColor
    readonly property color messageStatusAlertColor: theme.messageStatusAlertColor
    readonly property color messageStatusAlertTextColor: theme.messageStatusAlertTextColor

    readonly property int headerSize: 50
    readonly property int usernameSize: 30
    readonly property int primaryTextSize: 25
    readonly property int tabSize: 25
    readonly property int chatSize: 20
    readonly property int secondaryTextSize: 20 // address
    readonly property int chatMetaTextSize: 15
    readonly property int badgeTextSize: 12
    readonly property int statusTextSize: 12
    readonly property int contactPortraitSize: 75

    readonly property int sidePaneMinSize: 700
    readonly property int doublePaneMinSize: 1000

    property ThemeType dark: CwtchDark{}
    property ThemeType light: CwtchLight{}
    property ThemeType theme: gcd.theme == "dark" ? dark : light
}
