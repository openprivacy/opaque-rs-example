import QtQuick 2.7

import QtQuick.Controls 2.13
import "theme"
import "fonts"

RadioButton {
    id: control

    property color textColor: Theme.mainTextColor
    property color indicatorColor: Theme.boxCheckedColor
    //property real size: 12
    spacing: 0 //4 * gcd.themeScale


    indicator: Rectangle {
        width: 16 * gcd.themeScale
        height: 16 * gcd.themeScale
        anchors.verticalCenter: parent.verticalCenter
        radius: 9 * gcd.themeScale
        border.width: 1

        Rectangle {
            anchors.fill: parent
            visible: control.checked
            color: indicatorColor
            radius: 9 * gcd.themeScale
            anchors.margins: 4 * gcd.themeScale
        }
    }

    contentItem: ScalingLabel {
        size: Theme.chatMetaTextSize
        color: textColor
        text: control.text
        font.family: Fonts.applicationFontRegular.name
        font.styleName: "Bold"
        leftPadding: control.indicator.width + control.spacing
    }

}
