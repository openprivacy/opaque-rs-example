import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import "styles"
import "." as Widgets
import "theme"
import "../opaque/fonts"
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: crItem
    implicitHeight: Theme.contactPortraitSize * logscale + 3
    height: implicitHeight

    property real logscale: 4 * Math.log10(gcd.themeScale + 1)
    property string displayName
    property alias image: portrait.source
    property string handle
    property bool isActive
    property bool isHover
    property string tag // profile version/type

    property color rowColor: Theme.backgroundMainColor
    property color rowHilightColor: Theme.backgroundHilightElementColor
    property alias portraitBorderColor: portrait.portraitBorderColor
    property alias portraitColor: portrait.portraitColor
    property alias nameColor: cn.color
    property alias onionColor: onion.color
    property alias onionVisible: onion.visible

    property alias badgeColor: portrait.badgeColor
    property alias badgeVisible: portrait.badgeVisible
    property alias badgeContent: portrait.badgeContent

    property alias hoverEnabled: buttonMA.hoverEnabled

    // Ideally read only for icons to match against
    property alias color: crRect.color

    property alias content: extraMeta.children

    signal clicked(string handle)

    Rectangle { // CONTACT ENTRY BACKGROUND COLOR
        id: crRect
        anchors.left: parent.left
        anchors.right: parent.right
        height: crItem.height
        width: parent.width
        color: isHover ? crItem.rowHilightColor : (isActive ? crItem.rowHilightColor : crItem.rowColor)

        Portrait {
            id: portrait
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 25 * logscale
        }

        ColumnLayout {
            id: portraitMeta

            anchors.left: portrait.right
            anchors.right: parent.right
            anchors.leftMargin: 4 * logscale
            anchors.verticalCenter: parent.verticalCenter

            spacing: 2 * gcd.themeScale


            EllipsisLabel { // CONTACT NAME
                id: cn
                anchors.right: parent.right
                anchors.left: parent.left
                size: Theme.usernameSize * gcd.themeScale
                font.family: Fonts.applicationFontExtraBold.name
                font.styleName: "ExtraBold"
                text: displayName
            }

            EllipsisLabel { // Onion
                id: onion
                text: handle
                anchors.right: parent.right
                anchors.left: parent.left
                size: Theme.secondaryTextSize * gcd.themeScale
            }

            onWidthChanged: {
                cn.textResize()
                onion.textResize()
            }

        }

        Column {
            id: extraMeta
            anchors.left: portraitMeta.right
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    MouseArea {  // Full row mouse area triggering onClick
        id: buttonMA
        anchors.fill: parent
        hoverEnabled: true

        onClicked: { crItem.clicked(crItem.handle) }

        onEntered: {
            isHover = true
        }

        onExited: {
            isHover = false
        }
    }

    Connections { // UPDATE UNREAD MESSAGES COUNTER
        target: gcd

        onResetMessagePane: function() {
            isActive = false
        }

        onUpdateContactDisplayName: function(_handle, _displayName) {
            if (handle == _handle) {
                displayName = _displayName
            }
        }

        onUpdateContactPicture: function(_handle, _image) {
            if (handle == _handle) {
                image = _image
            }
        }
    }
}
