import QtQuick 2.12
import "theme"

Column {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    Rectangle {
        height: 10
        color:"transparent"
        width: parent.width
    }


    Rectangle {
        anchors.horizontalCenter: parent.horizontalCenter
        height: 1
        width: parent.width * 0.95
        color: Theme.dropShadowColor
    }


    Rectangle {
        height: 10
        color:"transparent"
        width: parent.width
    }
}
