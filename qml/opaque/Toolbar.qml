import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "fonts/Twemoji.js" as T
import "." as Widgets
import "theme"
import "../opaque/fonts"


Rectangle { // Global Toolbar
    id: toolbar

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    height: 35 * gcd.themeScale


    Layout.minimumHeight: height
    Layout.maximumHeight: height
    color: Theme.toolbarMainColor

    property alias leftMenuVisible: btnLeftMenu.visible
    property alias backVisible: btnLeftBack.visible
    property alias rightMenuVisible: btnRightMenu.visible

    property alias titleWidth: paneArea.width


    signal leftMenu()
    signal back()
    signal rightMenu()

    Icon {
        id: btnLeftMenu
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/menu-24px.svg"


        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        width: 30
        height: 30

        onClicked: { leftMenu() }
    }

    Icon {
        id: btnLeftBack
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/chevron_left-24px.svg"

        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        width: 30
        height: 30

        onClicked: { back() }
    }



    Rectangle {
        id: paneArea
        anchors.right: parent.right

        EllipsisLabel {
            id: paneTitle

            visible: true
            anchors.horizontalCenter: parent.horizontalCenter

            color: Theme.mainTextColor
            size: Theme.tabSize * gcd.themeScale
            weight: Font.Bold
            font.family: Fonts.applicationFontRegular.name
            font.styleName: "Bold"
            text: "global toolbar"

            //extraPadding: btnRightMenu.width + 10

        }

        onWidthChanged: { paneTitle.textResize() }
    }

    Icon {
        id: btnRightMenu
        iconColor: Theme.toolbarIconColor
        source: gcd.assetPath + "core/more_vert-24px.svg"

        visible: false
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        width: 30
        height: 30

        onClicked: { rightMenu() }
    }

    function setTitle(text, width) {
        paneTitle.text = text
        paneArea.width = theStack.width
        paneTitle.textResize()
        paneTitle.visible = true
    }

    function hideTitle() {
        paneTitle.visible = false
    }


    Connections {
        target: gcd

        onSetToolbarTitle: function(handle) {
            setTitle(handle)
            btnRightMenu.visible = true
        }
    }

}
