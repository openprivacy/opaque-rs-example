# Opaque
Open Privacy's Awesome Qt-based User Experience Library!

## Usage
Add the Opaque widgets as a submodule wherever your QML files are stored:

```
cd qml
git submodule add https://git.openprivacy.ca/openprivacy/opaque.git
git submodule init
git submodule update
```

Compile as normal. :)

## Translations

Currently the EmojiDrawer is the only widget that needs translations, and it hasn't been fully ported into the translation system yet.

To use translations, install the appropriate `i18n/*` files as Qt translators. For example, in Cwtch we load translations as follows:

```
gcd.OpaqueTranslator = core.NewQTranslator(nil)
gcd.OpaqueTranslator.Load("translation_"+core.QLocale_System().Name(), ":/qml/opaque/i18n/", "", "")
core.QCoreApplication_InstallTranslator(gcd.OpaqueTranslator)
```

## Widgets

Documentation coming soon. :)