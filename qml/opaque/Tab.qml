import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "fonts/Twemoji.js" as T
import "theme"
import "fonts"
import "." as Opaque

Rectangle {
    id: root

    property bool active: false
    property alias text: label.text

    color: Theme.backgroundMainColor

    signal clicked

    height: label.height


    Opaque.ScalingLabel {
        id: label
        anchors.horizontalCenter: parent.horizontalCenter
        color: root.active ? Theme.mainTextColor : Theme.altTextColor
        size: Theme.tabSize

        font.family: Fonts.applicationFontRegular.name
        font.styleName: "Bold"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            parent.focus = true
            parent.clicked()
        }
    }
}
