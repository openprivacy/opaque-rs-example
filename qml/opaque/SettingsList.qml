import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "../opaque" as Opaque
import "../opaque/styles"
import "../opaque/theme"


ColumnLayout { // settingsList
    id: root
    anchors.right: parent.right
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.topMargin: 20

    property alias settings: flick.children

    Flickable {
        anchors.fill: parent
        id: flick

        boundsBehavior: Flickable.StopAtBounds
        clip:true

        contentWidth: root.width
        contentHeight: root.height

        // Settings go here in a col
    }
}



