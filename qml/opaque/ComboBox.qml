import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12
import QtGraphicalEffects 1.12
import "theme"



// Assumes data comes in a model like
//     model: ListModel {
//            ListElement { text: qsTr("locale-en"); value: "en" } }
ComboBox {
    id: control
    height: 30 * gcd.themeScale

    // visible item
    contentItem:  ScalingLabel {
        id: displayItem
        leftPadding: 10 * gcd.themeScale

        text: control.model.get(control.currentIndex)["text"]
        font: control.font
        color: Theme.mainTextColor
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {
        radius: control.height / 4
        color: Theme.backgroundMainColor
        border.color: Theme.backgroundMainColor

        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            horizontalOffset: 0
            verticalOffset: 0
            samples: 10
            radius: 8
            color: Theme.dropShadowColor
        }
    }

    // activate arrow button
    indicator: Rectangle {
        height: control.height
        width: control.height
        anchors.right: control.right
        radius: control.height / 4
        color: Theme.backgroundPaneColor
        border.color: Theme.backgroundPaneColor

        layer.enabled: true
        layer.effect: DropShadow {
            transparentBorder: true
            horizontalOffset: 0
            verticalOffset: 0
            samples: 10
            radius: 8
            color: Theme.dropShadowColor
        }

        Icon {
            iconColor: Theme.toolbarIconColor
            source: gcd.assetPath + "core/chevron_left-24px.svg"

            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.height * 0.75
            height: parent.height * 0.75
            rotationAngle: 270

            onClicked: { popup.visible = true }
        }
    }

    // items in the popup
    delegate: ItemDelegate {
        width: control.width
        highlighted: control.highlightedIndex === index
        height: ciText.height + (4 * gcd.themeScale)

        contentItem:
        Rectangle {
            anchors.fill: parent
            color: control.highlightedIndex === index ? Theme.backgroundHilightElementColor : Theme.backgroundMainColor
            ScalingLabel {
                id: ciText
                anchors.verticalCenter: parent.verticalCenter
                anchors.left:parent.left
                anchors.leftMargin: 10 * gcd.themeScale

                text: model["text"] //control.textRole ? (Array.isArray(control.model) ? modelData[control.textRole] : model[control.textRole]) : modelData
                color: Theme.mainTextColor
                font: control.font
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
        }

    }

    popup: Popup {
        y: control.height - 1
        width: control.width
        implicitHeight: contentItem.implicitHeight + 2
        padding: 1

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            radius: 2
            border.color: Theme.backgroundMainColor

            layer.enabled: true
            layer.effect: DropShadow {
                transparentBorder: true
                horizontalOffset: 0
                verticalOffset: 0
                samples: 10
                radius: 8
                color: Theme.dropShadowColor
            }
        }
    }
}
