import QtQuick 2.7

import QtQuick.Controls 2.13
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.12
import "." as Widgets
import "theme"

// UnderlineTextField is a textfield styled as just an underline
TextField {
    property alias backgroundColor: bg.color

    color:  Theme.mainTextColor
    font.pixelSize: Theme.secondaryTextSize * gcd.themeScale * gcd.themeScale
    signal clicked
    smooth: true
    placeholderTextColor: Theme.altTextColor

    background: Rectangle {
        id: bg
        anchors.fill: parent
        color: Theme.backgroundMainColor
        border.color: color
    }

    Rectangle {
        id: bottomBar
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 2
        color: Theme.mainTextColor
    }
}
