pragma Singleton

import QtQuick 2.5

QtObject {

    property FontLoader applicationFontBold: FontLoader {
        id: opensansBold
        source: "qrc:/qml/opaque/fonts/opensans/OpenSans-Bold.ttf"
    }

    property FontLoader applicationFontExtraBold: FontLoader {
        id: opensansExtraBold
        source: "qrc:/qml/opaque/fonts/opensans/OpenSans-ExtraBold.ttf"
    }

    property FontLoader applicationFontRegular: FontLoader {
        id: opensansRegular
        source: "qrc:/qml/opaque/fonts/opensans/OpenSans-Regular.ttf"
    }

    property FontLoader applicationFontLight: FontLoader {
        id: opensansLight
        source: "qrc:/qml/opaque/fonts/opensans/OpenSans-Light.ttf"
    }

}
