import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import "../theme"

Rectangle {
    width: img.width + 10
    height: img.height + 5
    property string emoji
    color: selected ? Theme.defaultButtonActiveColor : Theme.backgroundPaneColor
    property bool selected
    property string locale


    Image {
        id: img
        anchors.centerIn:parent
        opacity: 1.0


        source: gcd.assetPath + "twemoji/72x72/" + emoji + ".png"


        MouseArea {
            anchors.fill: parent

            onClicked: {
                gcd.setLocale(locale)
            }
        }
    }

    Connections {
        target: gcd

        onSupplySettings: function(zoom, newLocale) {
            selected = newLocale == locale
        }
    }
}
