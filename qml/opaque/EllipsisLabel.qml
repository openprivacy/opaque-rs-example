import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0
import "." as Widgets
import "theme"


// Needs the parent to have an onWidthChanged that calls .textResize()
Item {
    property string text

    property alias color: label.color
    property alias size: label.font.pixelSize
    property alias weight: label.font.weight
    property alias strikeout: label.font.strikeout
    property alias font: label.font
    property alias topPadding: label.topPadding
    property alias leftPadding: label.leftPadding
    property int extraPadding: 0

    property Item container: parent

    height: textMetric.height
    width: textMetric.width + 10
    anchors.leftMargin: 10

    Label {
        id: label
        textFormat: Text.PlainText

        elide: Text.ElideRight
        text: textMetric.text
    }

    TextMetrics {
        id: textMetric
        text: text
        font: label.font
    }

    onTextChanged: {
        textResize()
    }

    function textResize() {
        textMetric.text = text
        var i = 2
        var containerWidth = container != null ? container.width : 50
        // - 30 for padding
        while (textMetric.width > containerWidth - ((30 + extraPadding) * gcd.themeScale) && textMetric.width > 50) {
            textMetric.text = text.slice(0, text.length - (i * 3)) + "..."
            i++
        }
    }

}
