import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick 2.12
import "theme"

// ToggleSwtch implements a stylized toggle switch. It requires the user create a function called onToggled to
// perform any additional operations needed to define the behavior of the toggle switch
Switch {
    property bool isToggled
    property var onToggled: function () { console.log("In Superclass") };

    style: SwitchStyle {
        handle: Rectangle {
            implicitWidth: 25
            implicitHeight: 25
            radius: width*0.5
            color: Theme.toggleColor
            border.color: isToggled  ? Theme.toggleOnColor  :Theme.toggleOffColor
            border.width:5
        }
        groove: Rectangle {
            implicitWidth: 50
            implicitHeight: 25
            radius: 25*0.5
            color: isToggled  ? Theme.toggleOnColor  :Theme.toggleOffColor
        }
    }

    onClicked: function() {isToggled = !isToggled; onToggled()}
}


