import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

import "." as Opaque
import "../opaque/styles"
import "../opaque/theme"


Column {
    id: tehcol

    width: parent.width - 20
    anchors.horizontalCenter: parent.horizontalCenter
    padding: 10
    spacing: 10
    property bool inline: true
    property bool last: false

    property alias label: settingLabel.text
    property alias description: settingDescriptionLabel.text
    property alias field: fieldContainer.children



    Grid {
        id: container
        columns: inline ? 2 : 1
        spacing: 10 * gcd.themeScale
        padding: 10 * gcd.themeScale

        width: parent.width

        property int gridWidth: inline ? (parent.width / 2) - (20 * gcd.themeScale) : parent.width - (20 * gcd.themeScale)

        anchors.horizontalCenter: parent.horizontalCenter

        Column {
            Opaque.ScalingLabel {
                id: settingLabel
                width: container.gridWidth
                color: Theme.mainTextColor
                size: Theme.secondaryTextSize * gcd.themeScale
                font.weight: Font.Bold
                visible: text != ""
            }


            Opaque.ScalingLabel {
                id: settingDescriptionLabel
                width: container.gridWidth
                color: Theme.mainTextColor
                size: Theme.chatMetaTextSize * gcd.themeScale
                visible: settingDescriptionLabel.text != ""
                topPadding:10
            }
        }


        Item {
            id: fieldContainer
            width: container.gridWidth
            // needs a height so !inline HLine "knows" how far down to be
            height: fieldContainer.children[0].height
        }

    }

    Opaque.HLine { visible: !last }
}
