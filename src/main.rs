extern crate qmetaobject;
use qmetaobject::*;
use cstr::*;
use std::env;
use std::cell::RefCell;

qrc!(resources, "" {
        "qml/main.qml",
        "qml/opaque/TextField.qml",
        "qml/opaque/Setting.qml",
        "qml/opaque/SettingsList.qml",
        "qml/opaque/ScalingLabel.qml",
        "qml/opaque/HLine.qml",
        "qml/opaque/ButtonTextField.qml",
        "qml/opaque/Button.qml",
        "qml/opaque/styles/CwtchTextFieldStyle.qml",
        "qml/opaque/theme/qmldir",
        "qml/opaque/theme/Theme.qml",
        "qml/opaque/theme/ThemeType.qml",
        "qml/opaque/theme/CwtchDark.qml",
        "qml/opaque/theme/CwtchLight.qml",
        "qml/opaque/fonts/qmldir",
        "qml/opaque/fonts/Fonts.qml",
        "qml/opaque/fonts/Twemoji.js",
        "qml/opaque/fonts/opensans/OpenSans-Light.ttf",
        "qml/opaque/fonts/opensans/OpenSans-Regular.ttf",
        "qml/opaque/fonts/opensans/OpenSans-ExtraBold.ttf",
        "qml/opaque/fonts/opensans/OpenSans-Bold.ttf",
        "assets/fontawesome/regular/trash-alt.svg",
});

#[derive(QObject,Default)]
struct gcd {
    base : qt_base_class!(trait QObject),
    os : qt_property!(QString; NOTIFY os_changed),
    os_changed: qt_signal!(),
    theme : qt_property!(QString; NOTIFY theme_changed),
    theme_changed: qt_signal!(),
    themeScale : qt_property!(f64; NOTIFY themeScale_changed),
    themeScale_changed: qt_signal!(),
    assetPath : qt_property!(QString; NOTIFY assetPath_changed),
    assetPath_changed: qt_signal!(),
}

fn main() {
    resources();
    let mut engine = QmlEngine::new();
    let mut gcd = gcd::default();
    gcd.os = env::consts::OS.into();
    gcd.theme = "dark".into();
    gcd.themeScale = 1.10;
    gcd.assetPath = "../../assets/".into();

    let gcd_ref = RefCell::new(gcd);

    engine.set_object_property("gcd".into(), unsafe {QObjectPinned::new(&gcd_ref)});
    engine.load_file("qrc:/qml/main.qml".into());
    engine.exec();

}