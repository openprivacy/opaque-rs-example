Proof of concept getting [Opaque](https://git.openprivacy.ca/openprivacy/opaque/)
 components working in Rust.

## Building

    LD_LIBRARY_PATH  = /path/to/Qt/<version>/<arch>/lib/
    PATH  += /path/to/Qt/<version>/<arch>/bin
    
    cargo run --release